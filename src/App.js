// import React from "react";

// class App extends React.Component {
//   render() {
//     console.log(this);
//     return (
//       <div>
//         <Counter count={2} sqrt={3} />
//       </div>
//     );
//   }
// }

// class Counter extends React.Component {
//   Add = () =>
//     this.setState((state) => (state.count = state.count += this.props.count));
//   Sqrt = () =>
//     this.setState((state) => (state.sqrt = state.sqrt *= this.props.sqrt));

//   state = {
//     count: 0,
//     sqrt: 2,
//   };
//   render() {
//     // conso[]le.log(this.props);

//     return (
//       <>
//         <button onClick={this.Add}>Add</button>
//         <h1>{this.state.count}</h1>
//         <button onClick={this.Sqrt}>Sqrt</button>
//         <h1>{this.state.sqrt}</h1>
//       </>
//     );
//   }
// }

// export default App;

// import React from "react";

// class App extends React.Component {
//   addNumber = (a) =>
//     this.setState((state) => (state.number = state.number += a));
//   minusNumber = (a) =>
//     this.setState((state) => (state.number = state.number - a));
//   state = {
//     number: 21,
//   };
//   render() {
//     // console.log(this);
//     return (
//       <div>
//         <h1>Appni ichidagi qiymat: {this.state.number}</h1>
//         <Counter
//           count={2}
//           sqrt={3}
//           add={this.addNumber}
//           minus={this.minusNumber}
//         />
//       </div>
//     );
//   }
// }

// class Counter extends React.Component {
//   Add = () =>
//     this.setState((state) => (state.count = state.count += this.props.count));
//   Sqrt = () =>
//     this.setState((state) => (state.sqrt = state.sqrt *= this.props.sqrt));

//   state = {
//     count: 0,
//     sqrt: 2,
//   };
//   render() {
//     return (
//       <>
//         <button onClick={this.Add}>Add</button>
//         <h1>{this.state.count}</h1>
//         <button onClick={this.Sqrt}>Sqrt</button>
//         <h1>{this.state.sqrt}</h1>
//         <button onClick={() => this.props.add(12)}>Add number</button>
//         <button onClick={() => this.props.minus(10)}>Subtraction</button>
//       </>
//     );
//   }
// }

import React from "react";
import Card from "./card";
import CardSecond from "./card2";
import CardThird from "./card3";

function App() {
  return (
    <div
      className="App"
      style={{
        display: "flex",
        flexWrap: "wrap",
        alignItems: "center",
        justifyContent: "center",
        gap: "50px",
      }}
    >
      <Card />
      <CardSecond />
      <CardThird />
    </div>
  );
}

export default App;
