import { Component } from "react";
import download from "./needd.jpg";

class CardSecond extends Component {
  render() {
    return (
      <>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            background: "#020B4A",
            padding: "30px 60px",
            borderRadius: "15px",
            marginTop: "100px",
          }}
        >
          <img
            src={download}
            alt={"img"}
            style={{
              marginTop: "-100px",
              width: "150px",
              boxSizing: "border-box",
              padding: "8px",
              borderRadius: "100%",
              background: "linear-gradient(45deg, #FFFF00 0%, #800080 100%)",
            }}
          />
          <div
            className="about"
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <h5
              style={{
                color: "white",
                fontSize: "20px",
                fontFamily: "cursive",
              }}
            >
              Thomas Scheer
            </h5>
            <p
              style={{
                color: "gray",
                fontSize: "15px",
                fontFamily: "serif",
              }}
            >
              Digital Marketer
            </p>
          </div>

          <div
            className="aboutItem"
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
              paddingBottom: "30px",
              borderBottom: "1px solid gray",
            }}
          >
            <h5
              style={{
                color: "white",
                fontSize: "20px",
                fontFamily: "cursive",
              }}
            >
              About Me
            </h5>
            <p
              style={{
                color: "gray",
                fontSize: "15px",
                fontFamily: "serif",
                textAlign: "center",
              }}
            >
              I like jazz music and bacon.Learning new <br /> things is one of
              my obsessions.
            </p>
          </div>
          <button
            style={{
              marginTop: "35px",
              borderRadius: "15px",
              paddingTop: "10px",
              paddingBottom: "10px",
              paddingLeft: "20px",
              paddingRight: "20px",
              background: "#606896",
              border: "transparent",
              color: "white",
              cursor: "pointer",
            }}
          >
            ADD USER
          </button>
        </div>
      </>
    );
  }
}

export default CardSecond;
